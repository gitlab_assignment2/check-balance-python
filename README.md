# Check Balance

This repository contains a Python program that checks whether an expression containing brackets is balanced or not.

## Description

The Check Balance program takes an expression as a command-line argument and determines if the brackets in the expression are balanced or not. It uses a stack-based algorithm to match opening and closing brackets and validates the balance of the expression.

Valid entries for the check_balance.py program:

Only brackets (round brackets (), curly brackets {}, and square brackets []) are allowed in the expression.
Numeric digits, alphabets, and special characters are not allowed in the expression except brackets.
For example, the following expressions are considered valid:

- {}
- ()[]{}
- ([]{})

On the other hand, the following expressions are considered invalid: {Program will return <not balanced>} for these input entries


- abc
- 123
- [@]
- (abc)
- {123}

## Features

- Supports checking the balance of expressions containing round brackets `()`, curly brackets `{}`, and square brackets `[]`.
- Provides a command-line interface to run the program and check the balance of an expression.
- Includes unit tests using pytest to validate the correctness of the program.
- Included .gitlab-ci.yml file to run the check_balance.py file using the GitLab CI/CD Pipeline.

## Prerequisites

- Python (version 3.X or above)

## There are two ways to run this code. One run the code in local by cloning code in local and other using the gitlab CI/CD

### Run the program in local

1. Clone the repository:

   
   git clone https://gitlab.com/gitlab_assignment2/check-balance-python.git

   ![Screenshot](screenshot/clone.PNG)

2. Change into the project directory
    cd check-balance-python
    
    ![Screenshot](screenshot/cd.PNG)

3. Install the required dependencies:
    pip install -r requirements.txt

    ![Screenshot](screenshot/dependancy.PNG)

4. Run the Test cases
    ## Tests
    The project includes unit tests to ensure the correctness of the Check Balance program. To run the tests, use the following command:
    pytest test_check_balance.py

    ![Screenshot](screenshot/test.PNG)

    **1. Positive Test Case (test_positive_case):**

    This test case checks a balanced expression. The input string "{()}" represents a balanced expression containing round brackets. The expected output is "balanced". The test asserts that the captured output from running the program matches the expected output.

    **2. Negative Test Case (test_negative_case):**

    This test case checks an unbalanced expression. The input string "{(" represents an unbalanced expression where the opening and closing brackets do not match. The expected output is "not balanced". The test asserts that the captured output from running the program matches the expected output.

    **3. Alphanumeric Test Case (test_alphanumeric_case):**

    This test case checks an expression containing alphanumeric characters. The input string "*8**" contains alphanumeric characters along with special characters. Since the program is designed to validate the balance of brackets, the expected output is "not balanced". The test asserts that the captured output from running the program matches the expected output.

    These test cases cover different scenarios to ensure the Check Balance program functions correctly and produces the expected outputs for both balanced and unbalanced expressions.

5. Run the program to check of expression is balanced or not. 
To check the balance of an expression, run the program with the expression as a command-line argument:
    python check_balance.py <expression>
    Replace <expression> with the expression you want to check
    Example:
    python check_balance.py "{()}"

![Screenshot](screenshot/run.PNG)

A balanced expression should have matching opening and closing brackets, while an unbalanced expression will have mismatched or missing brackets or contains special or alphanumeric characters.


### Run program using CI/CD
The CI/CD pipeline is triggered automatically whenever changes are pushed to the repository BUT in our case we need user input before running the pipleine, Hence we need to run the pipeline manually.

**The pipeline consists of the following stages:**

- **Build:** This stage builds the project and installs the required dependencies.

- **Test:** This stage runs the unit tests to validate the correctness of the check_balance program.

- **Deploy:** This stage deploys the program to the target environment or performs any necessary deployment tasks. In our case it will run the code and print the output in the logs if expression is balanced or not balanced.

**Below are the steps to run the pipeline**

- **Step 1:**
    Go to the repository home page by clicking on the below link:
        https://gitlab.com/gitlab_assignment2/check-balance-python.git

    ![Screenshot](screenshot/home.PNG)

- **Step 2:**
    Navigate to the "CI/CD" or "Pipelines" section, depending on your GitLab version.

    ![Screenshot](screenshot/Navigate_Pipeline.PNG)

- **Step 3:** 
    Locate the "Run Pipeline" button and click on it.

    ![Screenshot](screenshot/run_pipeline.PNG)

- **Step 4:** 
    On the pipeline configuration page, you will have an option to provide variables. Look for the section where you can add variables.
    

- **Step 5:** 
    Variable has already been created for user to run , Just add the expression value in 3 input text box for the Key "inputString" 

    ![Screenshot](screenshot/input.PNG)
- **Step 6:** 
    Click on the "Run Pipeline" button to manually trigger the pipeline.

    ![Screenshot](screenshot/run_button.PNG)

- **Step 7:** 
    GitLab will initiate the pipeline execution and pass the input string provided through the "input string" variable to your pipeline jobs.

    ![Screenshot](screenshot/run_stages.PNG)
- **Step 8:** 
    Monitor the pipeline progress and check the output of your job to see if the expression is balanced or not.
    You can click on each stages and check the status and logs.

    ![Screenshot](screenshot/run.PNG)

    - Build :
        Click on Build stage , You will get navigates towards the build logs.

        ![Screenshot](screenshot/build.PNG)

    - Test:
        Click on Test stage , You will get navigates towards the test logs.

        ![Screenshot](screenshot/test_run.PNG)

    - Deploy:
        Click on Deploy stage , You will get navigates towards the Deploy logs.
        
        ![Screenshot](screenshot/deploy.PNG)


## Contributing
Contributions to this project are welcome! If you encounter any issues or have suggestions for improvements, please feel free to submit a pull request or open an issue.

## Conclusion
In conclusion, the check_balance program is a Python solution that allows you to determine whether an expression containing brackets is balanced or not. By utilizing a stack-based algorithm, it efficiently matches opening and closing brackets, validating the balance of the expression.

To use the program locally, simply clone the repository, navigate to the project directory, and install the required dependencies. You can then run the program using the command python check_balance.py <expression>, replacing <expression> with the expression you want to check. The program will provide an output indicating whether the expression is balanced or not.

Alternatively, you can leverage the power of GitLab's CI/CD pipeline to automate the process. The included .gitlab-ci.yml file sets up the pipeline, consisting of the Build, Test, and Deploy stages. By manually triggering the pipeline and providing the input string through variables, you can conveniently validate the balance of an expression. The pipeline execution will generate logs indicating the result of the balance check.

With comprehensive unit tests in place, you can have confidence in the correctness of the Check Balance program. The tests cover various scenarios, including positive and negative cases, as well as expressions with alphanumeric characters. This ensures that the program functions as expected and produces accurate results.
