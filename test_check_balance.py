import pytest
from check_balance import CheckBalance
import sys

def test_positive_case(capsys):
    """
    Test case to verify the positive scenario where the expression is balanced.
    """

    processor = CheckBalance()
    input_string = "{()}"
    expected_output = "balanced"

    # Set the command-line argument to the input string
    sys.argv[1] = input_string
    processor.run()

    # Retrieve the captured output and compare with the expected output
    captured = capsys.readouterr()
    assert captured.out.strip() == expected_output


def test_negative_case(capsys):
    """
    Test case to verify the negative scenario where the expression is not balanced.
    """

    processor = CheckBalance()
    input_string = "{("
    expected_output = "not balanced"

    # Set the command-line argument to the input string
    sys.argv[1] = input_string
    processor.run()

    # Retrieve the captured output and compare with the expected output
    captured = capsys.readouterr()
    assert captured.out.strip() == expected_output


def test_aphanumeric_case(capsys):
    """
    Test case to verify the scenario where the expression contains alphanumeric characters.
    """

    processor = CheckBalance()
    input_string = "*8**"
    expected_output = "not balanced"

    # Set the command-line argument to the input string
    sys.argv[1] = input_string
    processor.run()

    # Retrieve the captured output and compare with the expected output
    captured = capsys.readouterr()
    assert captured.out.strip() == expected_output