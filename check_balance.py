from collections import deque
import sys, json, os
import re

class CheckBalance:
    def __init__(self):
        pass

    def isBalanced(self, exp):
        """
        Function to check if the given expression is balanced or not.
        It checks for the balanced usage of brackets in the expression.

        Args:
            exp (str): The expression to be checked for balance.

        Returns:
            bool: True if the expression is balanced, False otherwise.
        """

        # Define special characters that are allowed in the expression
        special_characters = "@!#$%^&*-~!#$%^&*_-+=|:;'<>.?/"

        # Create an empty stack to store opening brackets
        stack = []

        try:
            # Check for valid characters in the expression (alphanumeric and allowed special characters)
            if not re.match(r'[^a-zA-Z0-9' + re.escape(special_characters) + '"]+$', exp):
                return False

            for ch in exp:
                if ch in "({[":
                    stack.append(ch)
                elif ch in ")}]":
                    if not stack:
                        # If a closing bracket is encountered without a corresponding opening bracket, the expression is not balanced
                        return False

                    top = stack.pop()
                    if (top == "(" and ch != ")") or (top == "{" and ch != "}") or (top == "[" and ch != "]"):
                        # If a closing bracket does not match the expected opening bracket, the expression is not balanced
                        return False

            # If all opening brackets have their corresponding closing brackets, and the stack is empty, the expression is balanced
            return len(stack) == 0

        except Exception:
            # Exception occurred while checking the balance, consider it as not balanced
            return False

    def run(self):
        """
        Run the balance check on the input expression provided as a command-line argument.
        Print the result indicating whether the expression is balanced or not.
        """

        if len(sys.argv) < 2:
            print("Please provide the string as a command-line argument.")
            return

        input_string = sys.argv[1]

        if self.isBalanced(input_string):
            print('balanced')
        else:
            print('not balanced')

if __name__ == '__main__':
    processer = CheckBalance()
    processer.run()